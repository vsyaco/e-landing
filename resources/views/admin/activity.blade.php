<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <title>Activity - Admin</title>

    <link href="https://unpkg.com/tailwindcss@^2/dist/tailwind.min.css" rel="stylesheet">

</head>
<body class="antialiased">
<div
    class="relative flex items-top justify-center min-h-screen bg-gray-100 dark:bg-gray-900 sm:items-center py-4 sm:pt-0 dark:text-white">

    <div class="max-w-6xl mx-auto sm:px-6 lg:px-8">
        <div class="flex justify-center pt-8 sm:justify-start sm:pt-0">
            <h1 class="text-xl">Последняя активность</h1>
        </div>

        <div class="mt-8 bg-white dark:bg-gray-800 overflow-hidden shadow sm:rounded-lg">

            <table class="w-full divide-y divide-gray-200">
                <thead class="bg-gray-50">
                <tr>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs text-center font-medium text-gray-500 uppercase tracking-wider">
                        URL
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs text-center font-medium text-gray-500 uppercase tracking-wider">
                        Количество визитов
                    </th>
                    <th scope="col"
                        class="px-6 py-3 text-left text-xs text-center font-medium text-gray-500 uppercase tracking-wider">
                        Последнее посещение
                    </th>
                </tr>
                </thead>
                <tbody class="bg-white divide-y divide-gray-200">
                @foreach($activities as $activity)
                    <tr>
                        <td class="px-6 py-4">
                            {{ $activity['url'] }}
                        </td>

                        <td class="px-6 py-4 whitespace-nowrap text-center">
                            {{ $activity['total'] ?? ''}}
                        </td>

                        <td class="px-6 py-4 whitespace-nowrap text-center">
                            {{ $activity['datetime'] }}
                        </td>
                    </tr>
                @endforeach
                </tbody>
            </table>

        </div>

        <div class="mt-3">{{ $activities->links() }}</div>

    </div>
</div>
</body>
</html>
