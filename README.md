# Проект Landing

Основан на Laravel 8.*, PHP 8.1

URL: [https://landing.e.multidelo.com](https://landing.e.multidelo.com)
URL истории: [https://landing.e.multidelo.com/admin/activity](https://landing.e.multidelo.com/admin/activity) 

Использует https://github.com/sajya/client для запросов в формате JSON-RPC 2.0 (см. примечание)

Для дебага и отладки осознанно активирован глобально Laravel [Telescope](https://laravel.com/docs/8.x/telescope#introduction). 
На демо доступен по адресу: [https://landing.e.multidelo.com/telescope/](https://landing.e.multidelo.com/telescope/). 
Также осознанно у проекта на демо сделано окружение local для проверки и тестирования

Для отправки уведомлений о посещении используются очереди (в случае c демо Redis), чтобы не ждать подключения, обработки и ответа запросов. Можно было бы конечно сделать общение 2 проектов через Redis также 

## Локальная установка и запуск проекта

```bash
git clone https://gitlab.com/vsyaco/e-landing.git
cd e-landing
composer install
cp .env.example .env
# Указать в .env урл ендопоинта для JSON-RPC 2.0 запросов в ACTIVITY_ENDPOINT
./vendor/bin/sail up -d
./vendor/bin/sail bash #вход к контейнер с проектом
npm install && npm run dev
php artisan migrate
```

Можно заполнить базу фейковыми страницами:
```bash
./vendor/bin/sail php artisan db:seed
```

## Ключевые места проекта
- **app/Http/Controllers/PageController.php** - контроллер страниц проекта
- **app/Http/Middleware/MonitorActivity.php** - глобальный middleware для перехватывания и отправки запросов
- **app/Jobs/AddActivity.php** - обёртка для отправки активности через очередь
- **app/Services/ActivityService.php** - класс с набором функции для отправки и приёма данных от сервера Activity
- **app/Models/Page.php** - модель страниц (для фейковых страниц)
- **routes/web.php** - роуты для страниц и админки
- **database/factories/PageFactory.php** - заполнение фейковыми страницами (вызов из **database/seeders/DatabaseSeeder.php**)
- **resources/views/*** - blade-шаблоны страниц 

## Примечание

Проект использует https://github.com/sajya/client для обёртки запросов в формате json-rpc. Так как проект этого человека находится в статусе разработки, но тем не менее работоспособовен, то чтобы загрузить в другой проект необходимо
добавить с composer.json (см composer.json в проекте)
```json
"repositories": [
    {
        "type": "vcs",
        "url": "https://github.com/sajya/client"
    }
],
```
после чего запустить `composer require sajya/client`
