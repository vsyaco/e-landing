<?php

namespace App\Services;

use DateTime;
use Illuminate\Pagination\LengthAwarePaginator;
use Illuminate\Support\Facades\Http;
use Sajya\Client\Client;

class ActivityService
{

    /**
     * Create a new client.
     *
     * @return void
     */
    public function __construct()
    {
        $this->client = new Client(Http::timeout(5)->baseUrl(env('ACTIVITY_ENDPOINT')));
    }

    /**
     * Send page activity
     *
     * @param string $url
     * @param $datetime
     * @return void
     * @todo Add error checking later
     */
    public function addActivity(DateTime $datetime, string $url = ''): void
    {
        $this->client->execute(
            method: 'activity@add',
            params: [
                'url' => $url,
                'datetime' => $datetime
            ]);
    }

    /**
     * Get page activity
     *
     * @param int $page page number
     * @return LengthAwarePaginator
     * @todo Add error checking later
     */
    public function getActivity(int $page = 1): LengthAwarePaginator
    {
        $response = $this->client->execute(
            method: 'activity@get',
            params: [
                'page' => $page
            ])->result();

        return new LengthAwarePaginator(
            items: Collect($response['data'] ?? []),
            total: $response['total'] ?? 1,
            perPage: $response['per_page'] ?? 5,
            currentPage: $response['current_page'] ?? 1,
            options: [
                'path' => route('admin.activity')
            ]
        );

    }


}
