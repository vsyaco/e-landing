<?php

namespace App\Http\Controllers;

use App\Models\Page;
use App\Services\ActivityService;
use Illuminate\Http\Request;

class PageController extends Controller
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {
        return view('pages.index', [
            'pages' => Page::all()
        ]);
    }

    /**
     * Show page
     */
    public function show(Page $page)
    {
        return view('pages.show', [
            'page' => $page,
        ]);
    }

    /*
     * Admin activity
     */
    public function activity(Request $request)
    {

        $page = max($request->input('page'), 1);
        $activities = (new ActivityService)->getActivity($page);

        return view('admin.activity', [
            'activities' => $activities,
        ]);

    }
}
