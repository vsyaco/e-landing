<?php

namespace App\Jobs;

use App\Services\ActivityService;
use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Queue\SerializesModels;

class AddActivity implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Url
     *
     * @var string
     */
    protected $url;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($url = '')
    {
        $this->url = $url;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        (new ActivityService())->addActivity(
            datetime: now(),
            url: $this->url
        );
    }
}
